package com.example.yogesh.westweatherapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.yogesh.westweatherapp.data.CurrentlyWeather;
import com.example.yogesh.westweatherapp.data.WeatherData;
import com.example.yogesh.westweatherapp.service.JSONWeatherDataParse;
import com.example.yogesh.westweatherapp.service.WeatherUpdateCallback;
import com.example.yogesh.westweatherapp.service.WeatherUpdateService;

public class WeatherActivity extends AppCompatActivity implements WeatherUpdateCallback {

    private WeatherUpdateService weatherUpdateService;
    TextView locationView;
    TextView temperatureText;
    TextView currentSummaryText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        temperatureText = (TextView)findViewById(R.id.temperatureText);
        weatherUpdateService = new WeatherUpdateService(this);

        weatherUpdateService.httpUpdateWeatherData("");
    }

    public void WeatherUpdateSuccess(String sString ){
        WeatherData weatherData = JSONWeatherDataParse.getWeatherDetails(sString);
        //System.out.println("Weather Data: "+weatherData);
        float temparature = 0;
        System.out.println("Weather Summary: "+weatherData.getCurrentlyWeather().getSummary());
        System.out.println("Weather Temperature: "+weatherData.getCurrentlyWeather().getTemperature());
        temparature = weatherData.getCurrentlyWeather().getTemperature();
        System.out.print(temparature);
        //System.out.println("HourlyWeather: " +weatherData.getHourlyWeather().getSummary());
        temperatureText.setText(String.valueOf(weatherData.getCurrentlyWeather().getTemperature()));
        currentSummaryText.setText(weatherData.getCurrentlyWeather().getSummary());
    }

    public void WeatherUpdateFail(Exception e){

    }
}
