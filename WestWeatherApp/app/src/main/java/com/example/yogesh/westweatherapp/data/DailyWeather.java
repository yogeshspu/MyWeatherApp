package com.example.yogesh.westweatherapp.data;

import java.util.List;

/**
 * Created by HP on 30/08/2016.
 */
public class DailyWeather {

    private String summary;
    private String icon;
    private List<DailyWeatherData> dailyWeatherData;

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSummary() {
        return summary;
    }

    public String getIcon() {
        return icon;
    }

    public List<DailyWeatherData> getDailyWeatherData() {
        return dailyWeatherData;
    }

    public void setDailyWeatherData(List<DailyWeatherData> dailyWeatherData) {
        this.dailyWeatherData = dailyWeatherData;
    }
}
