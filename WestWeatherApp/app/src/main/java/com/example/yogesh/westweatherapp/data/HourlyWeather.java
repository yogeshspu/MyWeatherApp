package com.example.yogesh.westweatherapp.data;

import java.util.List;

/**
 * Created by HP on 30/08/2016.
 */
public class HourlyWeather {
    private String summary;
    private String icon;
    private List<HourlyWeatherData> hourlyWeatherData;

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<HourlyWeatherData> getHourlyWeatherData() {
        return hourlyWeatherData;
    }

    public void setHourlyWeatherData(List<HourlyWeatherData> hourlyWeatherData) {
        this.hourlyWeatherData = hourlyWeatherData;
    }

    public String getSummary() {

        return summary;
    }

    public String getIcon() {
        return icon;
    }

}
