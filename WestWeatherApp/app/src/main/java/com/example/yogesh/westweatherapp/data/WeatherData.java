package com.example.yogesh.westweatherapp.data;

import java.util.List;

/**
 * Created by HP on 30/08/2016.
 */
public class WeatherData {

    private double latitude;
    private double longitude;
    private String timezone;
    private int offset;
    private CurrentlyWeather currentlyWeather; //= new CurrentlyWeather();
    private HourlyWeather hourlyWeather; //= new HourlyWeather();
    private DailyWeather dailyWeather; //= new DailyWeather();

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setCurrentlyWeather(CurrentlyWeather currentlyWeather) {
        this.currentlyWeather = currentlyWeather;
    }

    public void setHourlyWeather(HourlyWeather hourlyWeather) {
        this.hourlyWeather = hourlyWeather;
    }

    public void setDailyWeather(DailyWeather dailyWeather) {
        this.dailyWeather = dailyWeather;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public int getOffset() {
        return offset;
    }

    public CurrentlyWeather getCurrentlyWeather() {
        return currentlyWeather;
    }

    public HourlyWeather getHourlyWeather() {
        return hourlyWeather;
    }

    public DailyWeather getDailyWeather() {
        return dailyWeather;
    }
}
