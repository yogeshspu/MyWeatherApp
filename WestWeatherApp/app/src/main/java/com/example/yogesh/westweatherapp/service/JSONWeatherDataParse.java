package com.example.yogesh.westweatherapp.service;

import com.example.yogesh.westweatherapp.data.CurrentlyWeather;
import com.example.yogesh.westweatherapp.data.DailyWeather;
import com.example.yogesh.westweatherapp.data.DailyWeatherData;
import com.example.yogesh.westweatherapp.data.HourlyWeather;
import com.example.yogesh.westweatherapp.data.HourlyWeatherData;
import com.example.yogesh.westweatherapp.data.WeatherData;
import com.example.yogesh.westweatherapp.utils.WeatherAppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 30/08/2016.
 */
public class JSONWeatherDataParse {

    public static WeatherData getWeatherDetails(String data){
        WeatherData weatherData= new WeatherData();
        CurrentlyWeather currentlyWeather = new CurrentlyWeather();
        HourlyWeather hourlyWeather = new HourlyWeather();
        DailyWeather dailyWeather = new DailyWeather();

    try {


        JSONObject jsonObject = new JSONObject(data);
        weatherData.setLatitude(WeatherAppConstants.getDoubleValue("latitude",jsonObject ));
        weatherData.setLongitude(WeatherAppConstants.getDoubleValue("longitude", jsonObject));
        weatherData.setTimezone(WeatherAppConstants.getString("timezone", jsonObject));

        JSONObject currentJSON = WeatherAppConstants.getJSONObject("currently",jsonObject);
        currentlyWeather.setTime(WeatherAppConstants.getIntValue("time",currentJSON ));
        currentlyWeather.setSummary(WeatherAppConstants.getString("summary", currentJSON));
        currentlyWeather.setIcon(WeatherAppConstants.getString("icon",currentJSON ));
        currentlyWeather.setPrecipIntensity(WeatherAppConstants.getFloatValue("precipIntensity", currentJSON));
        currentlyWeather.setPrecipProbability(WeatherAppConstants.getFloatValue("precipProbability", currentJSON));
        currentlyWeather.setPrecipType(WeatherAppConstants.getString("precipType", currentJSON));
        currentlyWeather.setTemperature(WeatherAppConstants.getFloatValue("temperature", currentJSON));
        currentlyWeather.setApparentTemperature(WeatherAppConstants.getFloatValue("apparentTemperature", currentJSON));
        currentlyWeather.setDewPoint(WeatherAppConstants.getFloatValue("dewPoint", currentJSON));
        currentlyWeather.setHumidity(WeatherAppConstants.getFloatValue("humidity", currentJSON));
        currentlyWeather.setWindSpeed(WeatherAppConstants.getFloatValue("windSpeed", currentJSON));
        currentlyWeather.setWindBearing(WeatherAppConstants.getFloatValue("windBearing", currentJSON));
        currentlyWeather.setCloudCover(WeatherAppConstants.getFloatValue("cloudCover", currentJSON));
        currentlyWeather.setPressure(WeatherAppConstants.getFloatValue("pressure", currentJSON));
        currentlyWeather.setOzone(WeatherAppConstants.getFloatValue("ozone", currentJSON));


        JSONObject hourlyJSON = WeatherAppConstants.getJSONObject("hourly",jsonObject);
        hourlyWeather.setSummary(WeatherAppConstants.getString("summary",hourlyJSON ));
        hourlyWeather.setIcon(WeatherAppConstants.getString("icon", hourlyJSON));

        List<HourlyWeatherData> hourlyWeatherDataList = new ArrayList<HourlyWeatherData>();
        JSONArray hourlyDataArray = hourlyJSON.getJSONArray("data");
        if(hourlyDataArray != null){
            System.out.println("hourlyData Length:" +hourlyDataArray.length());
            System.out.println("hourlyData Array:" +hourlyDataArray);
            for(int i =0; i<hourlyDataArray.length(); i++){
                JSONObject houdlyJsonData = hourlyDataArray.getJSONObject(i);
                System.out.println("hourlyJsonData : " +houdlyJsonData.toString());
                HourlyWeatherData hourlyWeatherData = new HourlyWeatherData();
                hourlyWeatherData.setTime(WeatherAppConstants.getIntValue("time", houdlyJsonData));
                hourlyWeatherData.setSummary(WeatherAppConstants.getString("summary", houdlyJsonData));
                hourlyWeatherData.setIcon(WeatherAppConstants.getString("icon",houdlyJsonData));
                hourlyWeatherData.setPrecipIntensity(WeatherAppConstants.getFloatValue("precipIntensity", houdlyJsonData));
                hourlyWeatherData.setPrecipProbability(WeatherAppConstants.getFloatValue("precipProbability", houdlyJsonData));
                hourlyWeatherData.setPrecipType(WeatherAppConstants.getString("precipType", houdlyJsonData));
                hourlyWeatherData.setTemperature(WeatherAppConstants.getFloatValue("temperature", houdlyJsonData));
                hourlyWeatherData.setApparentTemperature(WeatherAppConstants.getFloatValue("apparentTemperature", houdlyJsonData));
                hourlyWeatherData.setDewPoint(WeatherAppConstants.getFloatValue("dewPoint", houdlyJsonData));
                hourlyWeatherData.setHumidity(WeatherAppConstants.getFloatValue("humidity", houdlyJsonData));
                hourlyWeatherData.setWindSpeed(WeatherAppConstants.getFloatValue("windSpeed", houdlyJsonData));
                hourlyWeatherData.setWindBearing(WeatherAppConstants.getFloatValue("windBearing", houdlyJsonData));
                hourlyWeatherData.setCloudCover(WeatherAppConstants.getFloatValue("cloudCover",houdlyJsonData));
                hourlyWeatherData.setPressure(WeatherAppConstants.getFloatValue("pressure", houdlyJsonData));
                hourlyWeatherData.setOzone(WeatherAppConstants.getFloatValue("ozone", houdlyJsonData));
                hourlyWeatherDataList.add(hourlyWeatherData);
            }
            hourlyWeather.setHourlyWeatherData(hourlyWeatherDataList);
        }


        JSONObject dailyJSON = WeatherAppConstants.getJSONObject("daily",jsonObject);
        if(dailyJSON !=null){
            dailyWeather.setSummary(WeatherAppConstants.getString("summary",dailyJSON ));
            dailyWeather.setIcon(WeatherAppConstants.getString("icon", dailyJSON));

            List<DailyWeatherData> dailyWeatherDataList = new ArrayList<DailyWeatherData>();
            JSONArray dailyWeatherArray = dailyJSON.getJSONArray("data");

            if(dailyWeatherArray != null){
                System.out.println("dailyWeatherArray not null : " + dailyWeatherArray.length());

                for(int j=0; j<dailyWeatherArray.length(); j++){
                    try {
                        JSONObject DailyWeatherJSON = dailyWeatherArray.getJSONObject(j);
                        System.out.println("DailyWeather Data: " +DailyWeatherJSON.toString());
                        DailyWeatherData dailyWeatherData = new DailyWeatherData();
                        dailyWeatherData.setTime(WeatherAppConstants.getIntValue("time", DailyWeatherJSON));
                        dailyWeatherData.setSummary(WeatherAppConstants.getString("summary", DailyWeatherJSON));
                        dailyWeatherData.setIcon(WeatherAppConstants.getString("icon", DailyWeatherJSON));
                        dailyWeatherData.setSunriseTime(WeatherAppConstants.getIntValue("sunriseTime", DailyWeatherJSON));
                        dailyWeatherData.setSunsetTime(WeatherAppConstants.getIntValue("sunsetTime", DailyWeatherJSON));
                        dailyWeatherData.setMoonPhase(WeatherAppConstants.getFloatValue("moonPhase", DailyWeatherJSON));
                        dailyWeatherData.setPrecipIntensity(WeatherAppConstants.getFloatValue("precipIntensity", DailyWeatherJSON));
                        dailyWeatherData.setPrecipIntensityMax(WeatherAppConstants.getFloatValue("precipIntensityMax", DailyWeatherJSON));
                        dailyWeatherData.setPrecipIntensityMaxTime(WeatherAppConstants.getIntValue("precipIntensityMaxTime", DailyWeatherJSON));
                        dailyWeatherData.setPrecipProbability(WeatherAppConstants.getFloatValue("precipProbability", DailyWeatherJSON));
                        dailyWeatherData.setPrecipType(WeatherAppConstants.getString("precipType", DailyWeatherJSON));
                        dailyWeatherData.setTemperatureMin(WeatherAppConstants.getFloatValue("temperatureMin", DailyWeatherJSON));
                        dailyWeatherData.setTemperatureMinTime(WeatherAppConstants.getIntValue("temperatureMinTime", DailyWeatherJSON));
                        dailyWeatherData.setTemperatureMax(WeatherAppConstants.getFloatValue("temperatureMax", DailyWeatherJSON));
                        dailyWeatherData.setTemperatureMaxTime(WeatherAppConstants.getIntValue("temperatureMaxTime", DailyWeatherJSON));
                        dailyWeatherData.setApparentTemperatureMin(WeatherAppConstants.getFloatValue("apparentTemperatureMin", DailyWeatherJSON));
                        dailyWeatherData.setApparentTemperatureMinTime(WeatherAppConstants.getIntValue("apparentTemperatureMinTime", DailyWeatherJSON));
                        dailyWeatherData.setApparentTemperatureMax(WeatherAppConstants.getFloatValue("apparentTemperatureMax", DailyWeatherJSON));
                        dailyWeatherData.setApparentTemperatureMaxTime(WeatherAppConstants.getIntValue("apparentTemperatureMaxTime", DailyWeatherJSON));
                        dailyWeatherData.setDewPoint(WeatherAppConstants.getFloatValue("dewPoint", DailyWeatherJSON));
                        dailyWeatherData.setHumidity(WeatherAppConstants.getFloatValue("humidity", DailyWeatherJSON));
                        dailyWeatherData.setWindSpeed(WeatherAppConstants.getFloatValue("windSpeed", DailyWeatherJSON));
                        dailyWeatherData.setWindBearing(WeatherAppConstants.getFloatValue("windBearing", DailyWeatherJSON));
                        dailyWeatherData.setCloudCover(WeatherAppConstants.getFloatValue("cloudCover", DailyWeatherJSON));
                        dailyWeatherData.setPressure(WeatherAppConstants.getFloatValue("pressure", DailyWeatherJSON));
                        dailyWeatherData.setOzone(WeatherAppConstants.getFloatValue("ozone", DailyWeatherJSON));
                        dailyWeatherDataList.add(dailyWeatherData);
                    }catch (Exception e){
                        System.out.println("Exception Daily Weather: " + e);
                    }

                }
                dailyWeather.setDailyWeatherData(dailyWeatherDataList);
            }
            else {
                System.out.println("daily data json null");
            }

        }

        System.out.println("Setting currentlyWeather");
        weatherData.setCurrentlyWeather(currentlyWeather);

        System.out.println("Setting hourlyWeather");
        weatherData.setHourlyWeather(hourlyWeather);

        System.out.println("Setting dailyWeather");
        weatherData.setDailyWeather(dailyWeather);

        return  weatherData;

    }catch (Exception e){
        System.out.printf("YOGESH EXCE : ", e);
    }

        System.out.println("RETURNING NULL");
    return  null;
    }
}
