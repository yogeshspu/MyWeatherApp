package com.example.yogesh.westweatherapp.service;

import com.example.yogesh.westweatherapp.data.CurrentlyWeather;
import com.example.yogesh.westweatherapp.data.WeatherData;

/**
 * Created by HP on 30/08/2016.
 */
public interface WeatherUpdateCallback {

    void WeatherUpdateSuccess(String sData);

    void WeatherUpdateFail(Exception error);
}
