package com.example.yogesh.westweatherapp.service;

import android.os.AsyncTask;

import com.example.yogesh.westweatherapp.data.CurrentlyWeather;
import com.example.yogesh.westweatherapp.data.WeatherData;
import com.example.yogesh.westweatherapp.utils.WeatherAppConstants;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by HP on 30/08/2016.
 */
public class WeatherUpdateService {
    private double latitude =37.8267;
    private double longitude =-122.423;
    private WeatherUpdateCallback weatherUpdateCallback;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
    public WeatherUpdateService(WeatherUpdateCallback wcallback){
        this.weatherUpdateCallback = wcallback;
    }

    public void updateLocationdetails(){
        this.latitude =37.8267;
        this.longitude =-122.423;
    }

    public void httpUpdateWeatherData(final String location){
        new AsyncTask<String, Void, String>(){

            @Override
            protected String doInBackground(String... strings) {
                HttpURLConnection connection = null;
                InputStream inputStream = null;
                try{
                    connection= (HttpURLConnection)(new URL(WeatherAppConstants.URLLINK)).openConnection();
                    connection.setRequestMethod("GET");
                    connection.setDoInput(true);
                    connection.connect();

                    /* Read the Response data*/
                    StringBuffer stringBuffer = new StringBuffer();
                    inputStream = connection.getInputStream();
                     BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String readline = null;
                    while((readline = bufferedReader.readLine())!= null){
                        stringBuffer.append(readline+"\r\n");
                    }
                    return stringBuffer.toString();
                }catch (Exception e){
                    System.out.println("ERror: OCCURED");
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                //WeatherData weatherData;
                System.out.println(s);
                //weatherData = JSONWeatherDataParse.getWeatherDetails(s);
                //System.out.println(weatherData.getTimezone());
                weatherUpdateCallback.WeatherUpdateSuccess(s);

            }
        }.execute(location);
    }
}
