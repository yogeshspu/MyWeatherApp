package com.example.yogesh.westweatherapp.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by HP on 30/08/2016.
 */
public class WeatherAppConstants {

    public static final String URLLINK = "https://api.forecast.io/forecast/cb5ecf87bd42d63b9112b9a409e3c018/33.8688,151.2093";

    public static JSONObject getJSONObject(String string, JSONObject jsonObject)throws JSONException{
        JSONObject jObject = jsonObject.getJSONObject(string);
        return  jObject;
    }

    public static String getString(String key, JSONObject jsonObject)throws  JSONException{
        return jsonObject.optString(key);
    }

    public static float getFloatValue(String key, JSONObject jsonObject) throws JSONException{

        return (float)jsonObject.optDouble(key);
    }

    public static double getDoubleValue(String key, JSONObject jsonObject) throws JSONException{
        return jsonObject.optDouble(key);
    }

    public  static int getIntValue(String key, JSONObject jsonObject) throws JSONException{
        return jsonObject.optInt(key);
    }

    public static  long getLongValue(String key, JSONObject jsonObject)throws  JSONException{
        return  jsonObject.optLong(key);
    }


}
